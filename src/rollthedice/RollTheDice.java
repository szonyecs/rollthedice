
package rollthedice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

/**
 *
 * @author Szonja
 */
public class RollTheDice {

    List<Dice> kockak;

    public RollTheDice(List list) {
        this.kockak = list;
    }

    public static void main(String[] args) {

        System.out.println("Hány kockával szeretnél játszani?");
        Scanner sc = new Scanner(System.in);
        int input = -1;
        boolean exception = false;
        do {
            exception = false;
            try {
                input = sc.nextInt();
                if (input <= 0) {
                    throw new InputMismatchException();
                }
            } catch (InputMismatchException e) {
                System.out.println("Pozitív egész számot adj meg.");
                exception = true;
            }
        } while (exception);

        List<Dice> list = new ArrayList<>();
        for (int i = 0; i < input; i++) {
            Dice kocka = new Dice(i + 1);
            list.add(kocka);
        }
        RollTheDice rolli = new RollTheDice(list);
        rolli.roll(input);
    }

    public void roll(int count) {
        List<Integer> list = new ArrayList<>();
        int result = 0;
        for (int i = 0; i < count; i++) {
            result = (int) (Math.random() * 6) + 1;
            System.out.println((i + 1) + ". számú kocka dobása: " + result);
            list.add(result);
        }
        System.out.println("A leggyakoribb dobás: " + mostCommon(list));

    }

    public <T> T mostCommon(List<T> list) {
        Map<T, Integer> map = new HashMap<>();
        for (T t : list) {
            Integer val = map.get(t);
            map.put(t, val == null ? 1 : val + 1);
        }

        Entry<T, Integer> max = null;

        for (Entry<T, Integer> entry : map.entrySet()) {
            if (max == null || entry.getValue() > max.getValue()) {
                max = entry;
            }
        }
        return max.getKey();
    }

}
